/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
 */

#ifndef ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRONCONTAINER_V1_H
#define ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRONCONTAINER_V1_H

#include "AthContainers/DataVector.h"
#include "AthenaPoolExampleData/versions/ExampleElectron_v1.h"

namespace xAOD {
typedef DataVector<ExampleElectron_v1> ExampleElectronContainer_v1;
}

#endif  // ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRON_V1_H
